<div align="center">
<img src="https://cdn.jsdelivr.net/gh/viptv-work/img@main/iptv/about-IPTV.jpg" height="200" />
<h1 > IPTV </h1> 
<h4>收集来自世界各地的公开可用的网络电视播放源</h4>  
<h3 >
<a href="https://iptv.vodtv.cn">主站</a> - <a href="https://cdn.jsdelivr.net/gh/vodtv/iptv@gh-pages/">镜像1</a> - <a href="https://fastly.jsdelivr.net/gh/vodtv/iptv@gh-pages/">镜像2</a>
</h3> 
</div>

---

## 目录

- ❓  [如何使用](#如何使用)
- 📺 [播放列表](#播放列表)
- 🚀 [CDN加速](#CDN加速)
- 🆕 [当前更新](#当前更新)
- 📖 [免责申明](#免责申明)

## 如何使用

只需将以下链接之一插入任何支持实时流式传输的视频播放器，然后按打开即可。

## 播放列表

| channel | url | list | count | isRollback |
| ------- | --- | ---- | ----- | ---------- |
<!-- channels_here -->

## CDN加速

### 本站 (iptv.vodtv.cn) - (主推)

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://iptv.vodtv.cn/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://iptv.vodtv.cn/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://iptv.vodtv.cn/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://iptv.vodtv.cn/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像站(vodtv.netlify.app) -（国内访问快）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>m3u</td>
      <td>https://vodtv.netlify.app/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://vodtv.netlify.app/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://vodtv.netlify.app/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://vodtv.netlify.app/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像站(raw.gitlink.org.cn) -（国内访问快）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>iptv</td>
      <td>https://www.gitlink.org.cn/api/iptv/iptv/raw/cn.iptv?ref=gh-pages</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://www.gitlink.org.cn/api/iptv/iptv/raw/txt/cn.txt?ref=gh-pages</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://www.gitlink.org.cn/api/iptv/iptv/raw/epg/51zmt.xml?ref=gh-pages</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://www.gitlink.org.cn/api/iptv/iptv/raw/sources/cn.json?ref=gh-pages</td>
    </tr>
  </tbody>
</table>

### 镜像站(fastly.jsdelivr.net || cdn.jsdelivr.net) -（国内访问快）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://fastly.jsdelivr.net/gh/vodtv/iptv@gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://fastly.jsdelivr.net/gh/vodtv/iptv@gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://fastly.jsdelivr.net/gh/vodtv/iptv@gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://fastly.jsdelivr.net/gh/vodtv/iptv@gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像站(cdn.gitmirror.com ) -（备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 原网址前加（ghproxy.net || mirror.ghproxy.com） -（国内访问快）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
   <tbody>
    <tr>
      <td>M3U</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

## 当前更新

 <!-- updated_here -->

## 免责申明

- 所有播放源均收集于互联网，仅供测试研究使用，不得商用。
- 通过 M3U8 Web Player 测试直播源需使用 https 协议的直播源链接。
- 部分广播电台节目播出具有一定的时效性，需要在指定时段进行收听。
- 本项目不存储任何的流媒体内容，所有的法律责任与后果应由使用者自行承担。
- 您可以 Fork 本项目，但引用本项目内容到其他仓库的情况，务必要遵守开源协议。
- 本项目不保证直播频道的有效性，直播内容可能受直播服务提供商因素影响而失效。

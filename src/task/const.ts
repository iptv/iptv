import path from "path"

export const config_path = path.resolve("config")

export const config_custom_path = path.join(config_path, "custom")

export const data_path = path.resolve("data")

export const write_custom_path = path.join(data_path, "custom")
